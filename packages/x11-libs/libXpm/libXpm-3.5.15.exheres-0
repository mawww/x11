# Copyright 2007 Alexander Færøy <ahf@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require xorg [ suffix=tar.xz ]
require autotools [ supported_autoconf=[ 2.7 ] supported_automake=[ 1.16 ] ]

SUMMARY="X Pixmap (XPM) image file format library"

LICENCES="X11"
SLOT="0"
PLATFORMS="~amd64 ~armv7 ~armv8 ~x86"
MYOPTIONS=""

DEPENDENCIES="
    build:
        sys-devel/gettext
        virtual/pkg-config
        x11-proto/xorgproto
        x11-utils/util-macros[>=1.16] [[ note = [ needed for autotools ] ]]
    build+run:
        x11-libs/libX11
        x11-libs/libXt
        x11-libs/libXext
    test:
        dev-libs/glib:2
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --disable-open-zfile
    --disable-static
)

DEFAULT_SRC_CONFIGURE_TESTS=(
    "--enable-unit-tests --disable-unit-tests"
)

src_prepare() {
    # The tests below fail because the files they expect aren't created,
    # because there's no compress executable
    edo sed \
        -e '/check_PROGRAMS/s/XpmCreate //' \
        -e '/check_PROGRAMS/s/XpmRead //' \
        -e '/check_PROGRAMS/s/XpmWrite//' \
        -i test/Makefile.am

    autotools_src_prepare
}

