# Copyright 2018, 2021 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require qt cmake [ ninja=true ]

export_exlib_phases src_compile src_install

SUMMARY="Qt Cross-platform application framework: Qt Data Visualization"
DESCRIPTION="Qt Data Visualization module provides a way to visualize data in
3D as bar, scatter, and surface graphs. It is especially useful for
visualizing depth maps and large quantities of rapidly changing data, such as
data received from multiple sensors. The look and feel of graphs can be
customized by using themes or by adding custom items and labels to them.
Qt Data Visualization is built on Qt 5 and OpenGL to take advantage of
hardware acceleration and Qt Quick 2."

LICENCES="GPL-3"

MYOPTIONS="examples"

DEPENDENCIES="
    build+run:
        x11-dri/mesa
        x11-libs/qtbase:${SLOT}[>=${PV}]
        x11-libs/qtdeclarative:${SLOT}[>=${PV}]
"

CMAKE_SRC_CONFIGURE_OPTIONS+=(
    'examples QT_BUILD_EXAMPLES'
)

qtdatavis3d-6_src_compile() {
    cmake_src_compile

    option doc && eninja docs
}

qtdatavis3d-6_src_install() {
    cmake_src_install

    option doc && DESTDIR="${IMAGE}" eninja install_docs
}

